package com.example.vagap.testtask;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.example.vagap.testtask.Timer;
import com.example.vagap.testtask.TimerDisplay;

public class ReverseTimer extends ActionBarActivity {

	private Timer m_timer;

	private static final String PREFERENCESNAME = "Test task";
	private static final String TIMERVALUE = "SaveTime";

	private SharedPreferences m_preferences;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reverse_timer);

        Button m_startButton = (Button) findViewById(R.id.StartButton);
        Button m_stopButton = (Button) findViewById(R.id.StopButton);
        Button m_resetButton = (Button) findViewById(R.id.ResetButton);

        EditText minute = (EditText) findViewById(R.id.MinuteEdit);
        EditText second = (EditText) findViewById(R.id.SecondEdit);
        EditText millisecond = (EditText) findViewById(R.id.MillisecondEdit);

        m_timer = new Timer(new TimerDisplay(minute, second, millisecond));
        m_preferences = getSharedPreferences(PREFERENCESNAME, 0);

        m_startButton.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		m_timer.start();
        	}
        });

        m_stopButton.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		m_timer.stop();
        	}
        });

        m_resetButton.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View view) {
        		m_timer.reset();
        	}
        });
    }

    @Override
	protected void onPause () {
		super.onPause();
		m_timer.stop();

		Editor editor = m_preferences.edit();
		editor.putLong(TIMERVALUE, m_timer.getTime());
		editor.commit();
	}

    @Override
    protected void onResume() {
    	super.onResume();

    	m_timer.setTime(m_preferences.getLong(TIMERVALUE, 0L));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.reverse_timer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
