/*
 * ����� ��� ������������ ����������� �������� ������� 
 */
package com.example.vagap.testtask;

import android.widget.EditText;

public class TimerDisplay {
	private EditText m_MinuteEdit;
	private EditText m_SecondEdit;
	private EditText m_MillisecondEdit;

	private TimerDisplay() {
	}

	public TimerDisplay(EditText MinuteEdit, EditText SecondEdit, EditText MillisecondEdit) {
		m_MinuteEdit = MinuteEdit;
		m_SecondEdit = SecondEdit;
		m_MillisecondEdit = MillisecondEdit;
	};

	public long getSettingTime() {
		Long second = Long.parseLong(m_SecondEdit.getText().toString());
		if (second >= 60) {
			second = 59L;
			m_SecondEdit.setText(Long.toString(second));
		}

		Long minute = Long.parseLong(m_MinuteEdit.getText().toString());
		if (minute >= 60) {
			minute = 59L;
			m_MinuteEdit.setText(Long.toString(minute));
		}

		return minute * 60 * 1000 + second * 1000 + Long.parseLong(m_MillisecondEdit.getText().toString());
	}

	public void setDisplayTime(long millisecond) {
		Long minute = millisecond / (60 * 1000);
		if (minute >= 60)
			millisecond = 59 * 60 * 1000 + millisecond % (60 * 1000);

		m_MinuteEdit.setText(String.format("%02d", millisecond / (60 * 1000)));
		m_SecondEdit.setText(String.format("%02d", (millisecond / 1000) % 60));
		m_MillisecondEdit.setText(String.format("%03d", millisecond % 1000));
	}
}
