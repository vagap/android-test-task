/*
 * ���������� ������ �������
 */
package com.example.vagap.testtask;

import android.os.Handler;
import android.os.SystemClock;

import com.example.vagap.testtask.TimerDisplay;

public class Timer {
	private long timeStartTimer;
	private long leftTimer;
	private TimerDisplay m_TimerDisplay;

	private boolean m_flagRunnable = false;
	private Handler customHandler = new Handler();

	private Timer() {
	}

	public Timer(TimerDisplay display) {
		m_TimerDisplay = display;
	}

	public void start() {
		if ((leftTimer = m_TimerDisplay.getSettingTime()) == 0)
			return;

		if (m_flagRunnable)
			return;
		m_flagRunnable = true;

		timeStartTimer = SystemClock.uptimeMillis();
		customHandler.postDelayed(updateTimerThread, 0);
	}

	public void stop() {
		if (!m_flagRunnable)
			return;

		customHandler.removeCallbacks(updateTimerThread);
		leftTimer = getRemainingTime();

		m_flagRunnable = false;
	}

	public void reset() {
		customHandler.removeCallbacks(updateTimerThread);
		m_TimerDisplay.setDisplayTime(0L);

		m_flagRunnable = false;
	}

	private long getRemainingTime() {
		long remainingTime = leftTimer - (SystemClock.uptimeMillis() - timeStartTimer);
		if (remainingTime < 0)
			remainingTime = 0;
		m_TimerDisplay.setDisplayTime(remainingTime);
		return remainingTime;
	}

	public void setTime(long time) {
		m_TimerDisplay.setDisplayTime(time);
	}

	public long getTime() {
		return m_TimerDisplay.getSettingTime();
	}

	private Runnable updateTimerThread = new Runnable() {
		public void run() {
			if (getRemainingTime() != 0)
				customHandler.postDelayed(this, 0);
			else
				m_flagRunnable = false;
		}
	};
}
